from django.conf.urls import url
from . import views
from django.views.decorators.csrf import csrf_exempt

# We are adding a URL called /home
urlpatterns = [
    url(r'emotions/$', views.emotionsView.as_view(), name='emotions'),
]