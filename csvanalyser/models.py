from __future__ import unicode_literals
from django.contrib.postgres.fields import JSONField
from django.db import models

# Create your models here.


class VideoEmotions(models.Model):	
	video_id = models.CharField(max_length=200, blank=False, null=False)
	json_file = JSONField()

	def __unicode__(self):
		return self.video_id

class AvgVideoEmotions(models.Model):
	video_id = models.CharField(max_length=200, blank=False, null=False)
	sum_json_file = JSONField(null=True, blank=True)
	records_count = models.IntegerField(default=1)
	avg_json_file = JSONField(null=True, blank=True)

	def __unicode__(self):
		return self.video_id




