from django.shortcuts import render

# Create your views here.
import json
import random
import string
import os

from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication
from rest_framework import renderers, parsers
import json
from csvanalyser.sampledata import test
from .models import VideoEmotions, AvgVideoEmotions

from rest_framework.authentication import SessionAuthentication, BasicAuthentication 

class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return


class emotionsView(APIView):
	"""
	View to create a new json job.
	"""
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	

	def post(self, request):
		
		# test = json.dumps(test)
		# video_id = request.POST.get('id')
		# replace video_id value with the post is value
		video_emotions = VideoEmotions.objects.create(video_id="test2",json_file=test)
		avg_video_emotions, created = AvgVideoEmotions.objects.get_or_create(video_id='test2')
		
		if not created:
			for i in range(len(avg_video_emotions.sum_json_file['frames'])):
				avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['smile'] += video_emotions.json_file['frames'][i]['person']['emotions']['smile']
				avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['surprise'] += video_emotions.json_file['frames'][i]['person']['emotions']['surprise']
				avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['negative'] += video_emotions.json_file['frames'][i]['person']['emotions']['negative']
				avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['attention'] += video_emotions.json_file['frames'][i]['person']['emotions']['attention']
			avg_video_emotions.records_count += 1
			avg_video_emotions.save()
		else:
			print "dksnfkjsnfksn"
			avg_video_emotions.sum_json_file = video_emotions.json_file
			avg_video_emotions.save()
		
		# avg_video_emotions = AvgVideoEmotions.objects.get(video_id='test2')
		avg_video_emotions.avg_json_file = video_emotions.json_file
		avg_video_emotions.save()
		for i in range(len(avg_video_emotions.sum_json_file['frames'])):
			avg_video_emotions.avg_json_file['frames'][i]['person']['emotions']['smile'] = (float(avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['smile']))/float(avg_video_emotions.records_count)
			avg_video_emotions.avg_json_file['frames'][i]['person']['emotions']['surprise'] = (float(avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['smile']))/float(avg_video_emotions.records_count)
			avg_video_emotions.avg_json_file['frames'][i]['person']['emotions']['negative'] = (float(avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['negative']))/float(avg_video_emotions.records_count)
			avg_video_emotions.avg_json_file['frames'][i]['person']['emotions']['attention'] = (float(avg_video_emotions.sum_json_file['frames'][i]['person']['emotions']['attention']))/float(avg_video_emotions.records_count)
		avg_video_emotions.save()
		response = {}
		response['avg_json_file'] = avg_video_emotions.avg_json_file
		response['sum_json_file'] = avg_video_emotions.sum_json_file
		response['count'] = avg_video_emotions.records_count
		return Response(response, status=200)

	def get(self, request):
		"""
		Handles get request for tasks list view and filter tasks
		"""
		#video_id = request.POST.get('id')
		avg_video_emotions = AvgVideoEmotions.objects.get(video_id='video_id')
		response = {}
		response['json_file'] = avg_video_emotions.json_file
		response['count'] = avg_video_emotions.records_count
		return Response(response, status=200)
