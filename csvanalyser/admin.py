from django.contrib import admin
from .models import VideoEmotions, AvgVideoEmotions
# Register your models here.
admin.site.register(VideoEmotions)
admin.site.register(AvgVideoEmotions)